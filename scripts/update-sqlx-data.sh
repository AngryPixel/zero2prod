#!/usr/bin/env bash

set -x
cargo clean
cargo sqlx prepare -- --bin zero2prod
