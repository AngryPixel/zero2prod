##################################################################################################
# Planning Stage
##################################################################################################
# usw cargo-chef to speed up compile times by caching dependencies build result
FROM rust as chef
WORKDIR /app

# We only pay the installation cost once, it will be cached from the second build onwards
RUN cargo install cargo-chef

FROM chef as planner
COPY . .
# Compute a lock-like file for our project
RUN cargo chef prepare --recipe-path recipe.json

##################################################################################################
# Builder Stage
##################################################################################################
FROM chef AS builder
WORKDIR /app

COPY  --from=planner /app/recipe.json recipe.json

# Build our project dependencies
RUN cargo chef cook --release --recipe-path recipe.json

# Copy all files in the current directory, to the docker image
COPY . .

# Disable SQLX connections, force offline mode
ENV SQLX_OFFLINE true

# Build code in release
RUN cargo build --release



##################################################################################################
# Runtime Stage
##################################################################################################
FROM debian:bullseye-slim AS runtime

# Use /app dir
WORKDIR /app

# install ldd
RUN apt update && apt install lld clang -y

# Install OpenSSL
RUN apt-get update -y \
    && apt-get install -y --no-install-recommends openssl \
    # Cleanup
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

# Copy compiled binary only
COPY --from=builder /app/target/release/zero2prod zero2prod

# Copy configuration files
COPY configuration configuration

ENV APP_ENVIRONMENT production
# Set executable for docker run
ENTRYPOINT ["./zero2prod"]
